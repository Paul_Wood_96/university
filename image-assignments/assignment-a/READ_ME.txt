The following assignment is written in JAVA 11. The directory where the read me is located will be referred to as the projects
$HOME_DIRECTORY:

-To run the program run the following command on Linux in $HOME_DIRECTORY:
java -jar target/assignment-a-0.0.1-SNAPSHOT.jar
-In windows run the following:
java -jar target\assignment-a-0.0.1-SNAPSHOT.jar


The program will run execute and populate the "images" directory found in projects $HOME_DIRECTORY.
dilatedImage.png as an example specifies the image dilated task 3 of the assignment

The program uses environment variables to customize the following values:
-Image name
-Threshold value
-Box size for erosion and detection (structure-size)
-Inverted image for edge detection results

To modify any of the following just add -D$ where $ is the name of the variable you would like to change. The list of
environment variable names can be found in application.properties file in src/main/resources starting at $HOME_DIRECTORY
as an example run the following command to use the bird image
java -Dimage.name=bird.png -jar target/assignment-a-0.0.1-SNAPSHOT.jar

a list of all images you can run in the program can be found in src/main/resources/images

You can also manually update the properties in this file you will then need to run maven install to rebuild the .jar file:
mvn install -DskipTests
