package paul.assignment.assignmenta.services.utilities;

import java.awt.Color;

import paul.assignment.assignmenta.properties.ImageProperties;

/**
 * Optional operation, will invert the results of the edge detection algorithm.
 * This implies edges will be marked as black lines on a white background instead  of the default
 * White edges and a black background.
 *
 * @author paul
 * @see ImageProperties
 */
public class InvertedImage {

	public static int valueOfInverted(int pixelValue) {
		return pixelValue == 0 ? new Color(255, 255, 255).getRGB() :
				new Color(0, 0, 0).getRGB();
	}

}
