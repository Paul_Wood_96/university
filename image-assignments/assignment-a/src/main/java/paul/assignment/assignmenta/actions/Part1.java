package paul.assignment.assignmenta.actions;

import java.awt.image.BufferedImage;

import io.vavr.Function3;
import lombok.RequiredArgsConstructor;
import paul.assignment.assignmenta.services.UniformThreshold;

import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class Part1 {

	private final UniformThreshold uniformThreshold;

	private final Function3<BufferedImage, Integer, UniformThreshold, BufferedImage> streamImage;

	BufferedImage uniformThreshold(BufferedImage greyImage) {
		return streamImage.apply(greyImage, BufferedImage.TYPE_BYTE_BINARY, uniformThreshold);
	}

}
