package paul.assignment.assignmenta.actions;

import java.awt.image.BufferedImage;

import io.vavr.Function3;
import lombok.RequiredArgsConstructor;
import paul.assignment.assignmenta.image.Task;
import paul.assignment.assignmenta.properties.ImageProperties;
import paul.assignment.assignmenta.services.SimpleEdgeDetectionUsingSubtraction;

import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class Part3 {

	private final Function3<BufferedImage, Integer, Task, BufferedImage> streamImage;

	private final ImageProperties imageProperties;

	BufferedImage getImageEdge(BufferedImage erodedImage, BufferedImage dilatedImage) {

		SimpleEdgeDetectionUsingSubtraction edgeImage = new SimpleEdgeDetectionUsingSubtraction(
				erodedImage, dilatedImage, imageProperties.isInvertEdge());

		return streamImage.apply(erodedImage, BufferedImage.TYPE_BYTE_BINARY, edgeImage);
	}
}
