package paul.assignment.assignmenta.properties;

import lombok.Data;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@Data
@ConfigurationProperties("image")
public class ImageProperties {

	private String name;

	private int uniformThreshold;

	private int structureSize;

	private boolean invertEdge;

}
