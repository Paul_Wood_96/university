package paul.assignment.assignmenta.image;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import javax.imageio.ImageIO;

import io.vavr.Function3;
import io.vavr.control.Option;
import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;
import paul.assignment.assignmenta.properties.ImageProperties;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;

@Configuration
@Slf4j
public class ImageOperations {

	@Bean
	public Function<Integer, Integer> createPixelValue() {
		return value -> new Color(value, value, value).getRGB();
	}

	@Bean
	public BiConsumer<BufferedImage, String> saveImage() {
		return (image, name) -> {
			FileSystemResource destination = new FileSystemResource(String.format("images/%s.%s",
					name, "png"));

			Try.of(() -> ImageIO.write(image, "png", destination.getFile()))
					.getOrElseThrow(e -> new IllegalArgumentException("Could not save file: "));
		};
	}

	@Bean
	public Supplier<ClassPathResource> loadPath(ImageProperties imageProperties) {
		return () -> {
			ClassPathResource imagePath =
					new ClassPathResource(String.format("images/%s", imageProperties.getName()));

			return Option.of(imagePath.isReadable())
					.map(b -> imagePath)
					.getOrElseThrow(() -> new IllegalArgumentException("Could not open image"));
		};
	}

	@Bean
	public Supplier<BufferedImage> loadImage(Supplier<ClassPathResource> loadPath) {
		return () -> Try.of(() -> ImageIO.read(loadPath.get().getURL()))
				.getOrElseThrow(() -> new IllegalArgumentException("Could not open image "));
	}

	@Bean
	public <T extends Task> Function3<BufferedImage, Integer, T, BufferedImage> streamImage() {
		return (image, type, operation) -> {
			BufferedImage newImage = new BufferedImage(image.getWidth(), image.getHeight(), type);

			IntStream.range(0, image.getWidth()).forEach(x -> {
				IntStream.range(0, image.getHeight()).forEach(y -> {
					int val = operation.performTask(image, x, y);
					newImage.setRGB(x, y, val);
				});
			});

			return newImage;
		};
	}
}
