package paul.assignment.assignmenta.image;


import java.awt.image.BufferedImage;

public interface Task {
	int performTask(BufferedImage image, int x, int y);
}
