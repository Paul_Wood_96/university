package paul.assignment.assignmenta.services;


import java.awt.image.BufferedImage;
import java.util.function.Function;

import lombok.RequiredArgsConstructor;
import paul.assignment.assignmenta.image.Task;
import paul.assignment.assignmenta.properties.ImageProperties;

import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UniformThreshold implements Task {

	private final ImageProperties imageProperties;

	private final Function<Integer, Integer> createPixelValue;

	@Override
	public int performTask(BufferedImage image, int x, int y) {

		int imageValue = image.getRGB(x, y) & 0xFF;

		if (imageValue <= imageProperties.getUniformThreshold()) {
			return createPixelValue.apply(0);
		}
		else {
			return createPixelValue.apply(255);
		}
	}
}
