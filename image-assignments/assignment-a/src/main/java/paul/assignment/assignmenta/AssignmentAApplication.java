package paul.assignment.assignmenta;


import paul.assignment.assignmenta.actions.PerformAssignment;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class AssignmentAApplication {

	public static void main(String[] args) {

		SpringApplication.run(AssignmentAApplication.class, args);
	}

	@Bean
	CommandLineRunner startPart1(PerformAssignment assignment) {
		return args -> assignment.part1();
	}


}
