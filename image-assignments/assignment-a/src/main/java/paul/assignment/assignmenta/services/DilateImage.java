package paul.assignment.assignmenta.services;

import java.awt.image.BufferedImage;
import java.util.function.Function;

import lombok.RequiredArgsConstructor;
import paul.assignment.assignmenta.image.Task;
import paul.assignment.assignmenta.properties.ImageProperties;

import org.springframework.stereotype.Component;

/**
 * Dilated image enhances brightness increasing he number of white pixels in the
 * binary image. The dilation algorithm is defined below
 *
 * Explore the structure around the pixel (ie. 3x3). Return an empty pixel (0,0,0)
 * if and only if every pixel around and inclusive does not have a value
 * else return a full pixel (255, 255, 255)
 *
 * @author paul
 * @see Task
 * @see ImageProperties
 */
@Component
@RequiredArgsConstructor
public class DilateImage implements Task {

	private final ImageProperties imageProperties;

	private final Function<Integer, Integer> createPixelValue;

	@Override
	public int performTask(BufferedImage image, int x, int y) {
		int structureSize = imageProperties.getStructureSize();
		int width = image.getWidth();
		int height = image.getHeight();

		for (int i = Math.max(x - structureSize, 0); i < Math.min(x + structureSize, width); i++) {
			for (int j = Math.max(y - structureSize, 0); j < Math.min(y + structureSize, height); j++) {
				if (shouldAddPixel(i, j, image)) {
					return createPixelValue.apply(255);
				}
			}
		}

		return createPixelValue.apply(0);
	}

	private boolean shouldAddPixel(int x, int y, BufferedImage image) {
		return image.getRaster().getSample(x, y, 0) != 0;
	}
}
