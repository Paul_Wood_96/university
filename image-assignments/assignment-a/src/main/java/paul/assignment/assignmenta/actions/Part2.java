package paul.assignment.assignmenta.actions;

import java.awt.image.BufferedImage;

import io.vavr.Function3;
import lombok.RequiredArgsConstructor;
import paul.assignment.assignmenta.image.Task;
import paul.assignment.assignmenta.services.DilateImage;
import paul.assignment.assignmenta.services.ErodeImage;

import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
class Part2 {

	private final ErodeImage erodeImage;

	private final DilateImage dilateImage;

	private final Function3<BufferedImage, Integer, Task, BufferedImage> streamImage;

	BufferedImage getErodedImage(BufferedImage binaryImage) {
		return streamImage.apply(binaryImage, BufferedImage.TYPE_BYTE_BINARY, erodeImage);
	}

	public BufferedImage getDilatedImage(BufferedImage binaryImage) {
		return streamImage.apply(binaryImage, BufferedImage.TYPE_BYTE_BINARY, dilateImage);
	}

}
