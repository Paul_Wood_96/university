package paul.assignment.assignmenta.services;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.function.Function;

import lombok.RequiredArgsConstructor;
import paul.assignment.assignmenta.image.Task;

import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ConvertToGreyScaleImage implements Task {

	private final Function<Integer, Integer> createPixelValue;

	@Override
	public int performTask(BufferedImage image, int x, int y) {
		Color pixel = new Color(image.getRGB(x, y));

		int red = (int) (pixel.getRed() * 0.299);
		int green = (int) (pixel.getGreen() * 0.587);
		int blue = (int) (pixel.getBlue() * 0.114);

		return createPixelValue.apply(red + green + blue);
	}
}
