package paul.assignment.assignmenta.services;

import java.awt.image.BufferedImage;

import lombok.Value;
import paul.assignment.assignmenta.image.Task;
import paul.assignment.assignmenta.services.utilities.InvertedImage;

@Value
public class SimpleEdgeDetectionUsingSubtraction implements Task {

	private BufferedImage erodedImage;

	private BufferedImage dilatedImage;

	private boolean isInverted;

	@Override
	public int performTask(BufferedImage image, int x, int y) {
		int pixelValue = erodedImage.getRaster().getSample(x, y, 0) - dilatedImage.getRaster().getSample(x, y, 0);

		if (isInverted) {
			return InvertedImage.valueOfInverted(pixelValue);
		}
		return pixelValue;
	}
}
