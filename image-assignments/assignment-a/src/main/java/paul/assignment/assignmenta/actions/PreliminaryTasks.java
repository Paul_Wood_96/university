package paul.assignment.assignmenta.actions;

import java.awt.image.BufferedImage;

import io.vavr.Function3;
import lombok.RequiredArgsConstructor;
import paul.assignment.assignmenta.services.ConvertToGreyScaleImage;

import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class PreliminaryTasks {

	private final ConvertToGreyScaleImage convertToGreyScaleImage;

	private final Function3<BufferedImage, Integer, ConvertToGreyScaleImage, BufferedImage> streamImage;

	public BufferedImage getGreyImage(BufferedImage originalImage) {
		return streamImage.apply(originalImage, BufferedImage.TYPE_BYTE_GRAY, convertToGreyScaleImage);
	}

}
