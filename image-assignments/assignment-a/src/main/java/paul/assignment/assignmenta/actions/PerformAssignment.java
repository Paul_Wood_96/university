package paul.assignment.assignmenta.actions;

import java.awt.image.BufferedImage;
import java.util.function.BiConsumer;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import paul.assignment.assignmenta.image.ImageOperations;
import paul.assignment.assignmenta.properties.ImageProperties;

import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
@Slf4j
public class PerformAssignment {

	private final Supplier<BufferedImage> loadImage;

	private final BiConsumer<BufferedImage, String> saveImage;

	private final PreliminaryTasks preliminaryTasks;

	private final Part1 part1;

	private final Part2 part2;

	private final Part3 part3;

	private final ImageProperties imageProperties;

	@SneakyThrows
	public void part1() {

		BufferedImage originalImage = loadImage.get();
		BufferedImage greyImage = preliminaryTasks.getGreyImage(originalImage);
		log.info("Successfully completed uniform threshold find image in images directory");
		saveImage.accept(greyImage, "greyImage");


		IntStream.range(0, 2).forEach(i -> {

			if(i % 2 == 0) {
				imageProperties.setInvertEdge(false);
			}
			else {
				imageProperties.setInvertEdge(true);
			}

			BufferedImage uniformImage = part1.uniformThreshold(greyImage);

			BufferedImage erodedImage = part2.getErodedImage(uniformImage);
			BufferedImage dilateImage = part2.getDilatedImage(uniformImage);
			log.info("Successfully completed erosion and dilation find image in images directory");

			BufferedImage edgeImage = part3.getImageEdge(erodedImage, dilateImage);
			log.info("Successfully extracted the edges of the image find the image in images directory");

			saveImage.accept(erodedImage, "erodedImage");
			saveImage.accept(uniformImage, "thresholdImage");
			saveImage.accept(dilateImage, "dilatedImage");
			saveImage.accept(edgeImage, String.format("%s_edgeImage", i));

		});
	}

}
