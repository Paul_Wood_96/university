package paul.assignment.assignmenta.services;

import java.awt.image.BufferedImage;
import java.util.function.Function;

import lombok.RequiredArgsConstructor;
import paul.assignment.assignmenta.image.Task;
import paul.assignment.assignmenta.properties.ImageProperties;

import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ErodeImage implements Task {

	private final ImageProperties imageProperties;

	private final Function<Integer, Integer> createPixelValue;

	@Override
	public int performTask(BufferedImage image, int x, int y) {
		int structureSize = imageProperties.getStructureSize();
		int width = image.getWidth();
		int height = image.getHeight();

		for (int i = Math.max(x - structureSize, 0); i < Math.min(x + structureSize, width); i++) {
			for (int j = Math.max(y - structureSize, 0); j < Math.min(y + structureSize, height); j++) {
				if (shouldRemovePixel(i, j, image)) {
					return createPixelValue.apply(0);
				}
			}
		}

		return createPixelValue.apply(255);

	}

	// should remove (set value to 0) a pixel only when all the pixels around given pixel
	// (according to structure size) are not equal to zero
	private boolean shouldRemovePixel(int x, int y, BufferedImage image) {
		return image.getRaster().getSample(x, y, 0) == 0;
	}
}
