<div align="center"><h1> IMY 772 Hypermedia and Mark-up </h1></div>

IMY 772 is structured around using, industry standard front-end development platforms such 
as Angular and AWS. The requirements for the course will be a single web-based system project. 
The web-based application must be developed using new generation web technologies, and deployed on 
AWS.

The project requires smart and innovative solutions to a simple social-media platform, targeted at 
young professionals. The project will be assessed on integration of required technologies, as well 
as usability testing user experience design and UI design. 

<hr>

<h3>Required Technologies</h3>

1. AWS 
2. Version Control like GitLabs
3. Time management application (Kanban boards???)
4. Front end frameworks ie. Angular/React
5. Serverless (Lambda)
6. real-time data use data in creative ways
7. Image processing capability
8. Cloud Storage solutions (RDS, DynamoDB)


<hr>

<div align="center"><h2>Website Name Goes Here One Day</h2></div> 
My Website proposed is an online tutor platform were students can sign up as private tutors or students.
The platform allows Tutors to advertise their services for free while allowing students to browse a 
selection of tutors or just ask general questions. Tutors should be able to broadcast schedules,  
what modules they are presenting on and also their rates. 

