<div align ="center"><h1>Templates in C++</h1></div> 

<h2>The Problem</h2>
creating flexible libraries, which would provide code for many different types without having to specify the type. 

<h2>Template</h2>

Designed to be constructs which could be defined without specifying their return type. 
When used in a program types would be specified during the call and the compiler would insert these types into the template 
and thus create a normal class or function out of it. A template is just that a template for a function or a class. 
Using templates it is possible to write programs that would be evaluated at compile-time. <br />
Writing simple programs in can become complicated in template-programming, however templating can 
provide elegant solutions to complicated iterative problems.   

*  Templates generate code at compile time based on types provided by the user. 
This means they ate great for coping with [combinatorial behaviors](https://en.wikipedia.org/wiki/Combinatorial_optimization).
*  Using templates gives you the granularity to create specialized implementations 
for specific class instantiations of a template. For example:
    *  ```SmartPtr<T>``` can be specialzed by ```SmartPtr<Widget>```
* You can make use of partial template splecializtions, this gives you the 
ability to specialize a class with only some of it arguments. For example: 

```c++
    
    // template 
    template <class T, class U> class SmartPtr { ... };
    
    // specialized without class T
    template <class U> class SmartPtr<Widget, U> { ... };
    
``` 

<h2>Example</h2>

Lets go over factorial example using *recursion*. 

```c++
    int factorial(int x) {
        if(x != 1) {
            return (x * (x-1))
        }   
        else retun 1;
    }
```

Template programming often follows a recursive style. This is because the compiler resolves recursive calls, templates are 
instantiated with another value and the compiler evaluates all new templates. <br />
Templates were originally designed to work with code that requires different types but what if we need to be specific in 
a specific type, like in this case. Our base case for a factorial recursive function was when `x` was a certain value. 
Templates can be defined with a specific value for a parameter, this technique is called *template specialization*. 
The template will then chose the most specific template it can find for a specific template call. <br />
Lets use a `class` to define our factorial template. However classes are not meant to return something, but they can 
store things so lets create a public member function to hold the result of our factorial call. First we can define the 
general case.

```c++
    template<int N>
    class Factorial {
    public:
        enum {
            // this would go on infinitely if we did not have a base case
            value = N * Factorial<N - 1>::value
        };
    };
``` 
In the code above we created a class named `Factorial` and a template parameter for an `int`. The general case will keep calling
Factorial N - 1 forever unless we define a specific case that does something other than keep calling Factorial, also quick 
note this isn't so much as calling as you are used to in *recursion* this is more closer related to initializing a 
new Factorial class with a new parameter. The general case, will then be created for any int value ie. 1, 2, 3, 4... untill 
we add the following specialized case: 

```c++
    template<>
    class Factorial<1> {
    public:
        enum {
            value = 1
        };
    };
    
``` 

What this does is actually quite simple, any time the Factorial class is initialized with 1 the compiler will specialize to 
this class and not the general case. This is a very similar concept to overloading functions but just imagine 
overloading functions by value not by type. That means this class acts as our base condition in the recursive call.  <br />
`value` in the original general case will then build all instances of Factorial classes until the value of N eventually trickles 
down to 1 where it is specialized with the above template. If there was no base case though `value` would never be assigned 
as the compiler would just keep instantiating new templates with new N values. <br />
This case is still very fragile. What happens when the original value sent in is < 1, well simple answer is a stack overflow.

<h2>Techniques</h2>

***Partial specializations*** <br />

Templates can take in more than a single parameter, allowing us to specialize on only one of those parameters, not both. 
This is commonly known as *partial specialization*. Let's look at an example that calculates the power function. 

```c++
    // structs work in the same way as classes, they can hold a value that will be treated as a return value
    template<int n, int m>
    struct power {
      enum {
          RET = power<n, m - 1>::RET * n
      };
    
    };
    
    template<int n>
    struct power<n, 1> {
      enum {
          RET = n
      };
    };
```

This is a handy tool, it saves us time from making single parameter templates to handle conditions. There are some restrictions 
on partial specialization however:
1) Most old compilers will not be able to handle it 
2) You can not partial specialize member or any other kind of function, only classes and struct levels.
3) You cannot specialize structure of a class (its data members). You can specialize only functions <br />

***The If-Construct*** <br />

Template conditions can be built making use of the partial techniques we just discovered earlier. The first parameter can be 
used as the condition, the second and third will represent the values that should be returned if the condition does or does not hold 
