//
// Created by paul on 2/13/20.
//

#include <iostream>

/**
 * Difficulties with partial specialization covered int this file. This is the basic 101 for generic programing
 * cover how templates are used and specialized
 *
 * @see understanding_partial_templates.cpp for a way to make use of partial specialization
 */
using namespace std;
/**
 * This template class can be initialized with any type/ class of type T.
 * This is a very generic case so nothing out of the ordinary happens
 *
 * @tparam T
 */
template<class T> class Test {
// must make the member function public to be called from the outside
public:
    void sayHello() {
        cout << "This is the generic implementation" << endl;
    }
};

/**
 * This implementation of the Test::sayHello method has been specialized for the
 * int type. All we have done is implement the member function of class test instead
 * of making use of the unspecified case above. This essentially means for this case
 * we wrote out the specific outcome. This is the function sayHello
 */
template<>
void Test<int>::sayHello() {
    cout << "This is the specialized implementation" << endl;
}

/**
 * What is meant by partial specialization. If a template has more than 1 parameter ie
 * in this case with gadget, it has two class parameters T and U. Partial specialization means
 * we cannot just specialize T and leave U generic, the same goes for U. If we want to specialize
 * we need to specialize all or both parameters.
 *
 * @tparam T
 * @tparam U
 */
template <class T, class U> class Gadget {
public:
    void fun() {
        cout << "This is a generic implementation with more than one template type" << endl;
    }
};

/**
 * Below is an attempt to only specialize T as type char and leave U generic. The compiler complains
 * and throws a partial specialization error.
 *
 */
//template <class U> void Gadget<char, U>::fun() {
//    cout << "This causes an error. Cannot partially specialize a member class of Gadget"
//}

/**
 * However here it is very easy to see how we would have specialized Gadget properly with both parameters
 * U and T specialized as char and int respectively.
 */
template <>
void Gadget<char, int>::fun() {
    cout << "Specialized both cases" << endl;
}




int main() {
    // test the general case first
    Test<string> nValue;
    nValue.sayHello();

    // then test the more specialized case where we implemented sayHello with int val
    Test<int> specialValue;
    specialValue.sayHello();

    cout << endl;

    // same as before test general case with any combinations of types
    Gadget<string, string> oValue;
    oValue.fun();

    // but as soon as the combination of types becomes the specialized case, the specialized function gets called.
    Gadget<char, int> fullySpecialized;
    fullySpecialized.fun();

    return 0;
};