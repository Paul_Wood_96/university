//
// Created by paul on 3/10/20.
//

#include <iostream>

/**
 * Partial specialization was not possible on a member function level but is possible on a class level. Actually
 * Partial specialization is not possible on any function member or global.
 *
 */
using namespace std;

/**
 * To recap this is a generalized template case. In this case Widget is created for
 * up to this point any unknown random compilation of types
 *
 * @tparam Window
 * @tparam Controller
 */
template <class Window, class Controller>
class Widget {
public:
    void fun() {
        cout << "Generic implementation" << endl;
    }
};

/**
 *  This is a very explicit implementation of the the Widget class for type <String, String>.
 */
template <>
class Widget<string, string> {
public:
    void newFun() {
        cout << "The entire Widget class can get structured differently by sending in different types" << endl;
    }
};

/**
 * Sometimes though you only want to send in one type like in this example. Window is left to the
 * compiler to decide and you can specialize just the Controller parameter. What's more is the
 * compiler is smart to chose correct specializations that are even partially specialized.
 * Look at the next example
 *
 * @return
 */
template <class Window>
class Widget<Window, int> {
public:
    void otherFun() {
        cout << "This crazy way of making classes by the parameters passed in insane" << endl;
    }

};

template <class Window>
class Widget<Window, char> {
public:
    void specificPartialFunction() {
        cout << "It is even smart enough to chose this class over the int class" << endl;
    }
};



int main() {

    // the most generic case with completely random non-specialized types
    Widget<long, long> genericClass;
    genericClass.fun();

    cout << endl;

    // this is our explicit class notice the newFun() on the Widget class. The same class different functions
    Widget<string, string> explicitClass;
    explicitClass.newFun();

    cout << endl;

    // and this is one of the partially specialized classed
    Widget<int, int> firstPartial;
    firstPartial.otherFun();
    // and this is it again partially implemented differently
    Widget<char, int> firstPartial2;
    firstPartial2.otherFun();

    cout << endl;

    // and finally here is the second partial
    Widget<int, char> secondPartial;
    secondPartial.specificPartialFunction();

    // what is important to notice in all cases in the different functions, that exist on the classes.
    // Each slightly more specific Widget implementation initialized completely different Widget classes

    return 0;
}