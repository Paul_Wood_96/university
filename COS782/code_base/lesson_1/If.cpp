//
// Created by paul on 2/12/20.
//
#include <iostream>
using namespace std;

// If as specified by Pisula
template <bool C, class ThenType, class ElseType>
struct If {
    typedef ThenType ret;
};

template <class ThenType, class ElseType>
struct If <false, ThenType, ElseType> {
    typedef ElseType ret;
};

//Using
struct Type1 {
    static inline void trueCase() {
        cout << "Pisula Statement 1" << endl;
    }
};

struct Type2 {
    static inline void falseCase() {
        cout << "Pisula Statement 2" << endl;
    }
};

int main() {

    // notice the different function options that are made available at run time
    If<(100 > 20), Type1, Type2>::ret::trueCase();
    If<(100 < 20), Type1, Type2>::ret::falseCase();

    return 0;
};
