//
// Created by paul on 3/28/20.
//

#include <iostream>
using namespace std;


/**
 * General case: template for any int value. Initializes other Factorial classes until it reasches
 * a specialized case.
 *
 * @tparam N int
 */
template<int N>
class Factorial {
public:
    enum {
        // this would go on infinitely if we did not have a base case, much like regular recursion
        value = N * Factorial<N - 1>::value
    };
};

/**
 * The specialized case. Has same class name as the general case but will be called when the int value
 * is 1. The compiler see this template given the specialization for the int value
 *
 */
template<>
class Factorial<1> {
public:
    enum {
        value = 1
    };
};



int main() {

    // Initialize factorial class
    cout << "4! is equal: " << Factorial<4>::value << endl;
    cout << "6! is equal: " << Factorial<6>::value << endl;

    return 0;
}