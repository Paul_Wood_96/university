#include <iostream>

using namespace std;
/**
 * A policy defines a class interface or a class template interface
 * consists of any combination of the following:
 *
 * 1) inner type definitions
 * 2) member functions
 * 3) member variables
 *
 * @return
 */

// first template class
template<class T>
struct OpNewCreator {
    static T* create() {
        return new T;
    }
};

// second template class
template<class T>
struct MallocCreator {
    static T* create() {
        void* buf = std::malloc(sizeof(T));
        if (!buf) return 0;
        return new(buf) T;
    }
};

// using simple template
template <class CreationPolicy>
class Test : public CreationPolicy {
    // implementations
};

//using a template template
template <template <class Created> class CreationPolicy>
class Test2 : public CreationPolicy<int>
{

};


int main() {

    // instantiated a Test template of type int
    typedef Test< OpNewCreator<int> > myTest;

    typedef Test2<OpNewCreator> myTest2;

    return 0;
}
