//
// Created by paul on 3/29/20.
//

#include <iostream>
using namespace std;


template<int n, int m>
struct power {
    enum {
        RET = power<n, m - 1>::RET * n
    };

};

template<int n>
struct power<n, 1> {
    enum {
        RET = n
    };
};


int main() {

    cout << "2 to the power of 4: " << power<2, 4>::RET << endl;
    cout << "4 to the power of 3: " << power<4, 3>::RET << endl;
    cout << "5 to the power of 2: " << power<5, 2>::RET << endl;

    return 0;
}
