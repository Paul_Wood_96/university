<div align=center><h1>COS782 Generic Programming</h1></div>

This module deals with code generation at compile time paying particular interest 
to design patterns. To this end, design patterns and compile-time programming 
techniques such as: basic compile-time programming constructs, object allocation, 
generalised functors, smart pointer and multi-methods are discussed in detail and 
applied to design patterns.
The modules follows the following textbook 

> C++ Design Generic Programming and Design Patterns Applied by Andrei Alexandrescu

<div align=center><h2>Notes</h2></div>

1) [Starting out with Templates](https://gitlab.com/Paul_Wood_96/university/-/blob/master/COS782/notes/Templates.md)


<div align=center><h2>Code base examples</h2></div>

> The module is primarily written in c++ and thus requires a g++ compiler. 

1) **Starting out with Template Examples**
    * [Understanding Templates](https://gitlab.com/Paul_Wood_96/university/-/blob/master/COS782/code_base/understanding_templates/understanding_templates.cpp)
    * [Understanding Partial Templates](https://gitlab.com/Paul_Wood_96/university/-/blob/master/COS782/code_base/understanding_templates/understanding_partial_templates.cpp)
2) **Lesson 1**
    * [Factorial](https://gitlab.com/Paul_Wood_96/university/-/blob/master/COS782/code_base/lesson_1/Factorial.cpp)
    * [Power function](https://gitlab.com/Paul_Wood_96/university/-/blob/master/COS782/code_base/lesson_1/power.cpp)